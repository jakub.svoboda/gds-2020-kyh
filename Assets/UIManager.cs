﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    private Animator animator;

    public void Transition()
    {
        Debug.Log("Triggering transition!");
        animator.SetTrigger("Fade");
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void OnFadeDone()
    {
        GameManager.Instance.OnFadeDone();
    }
}
