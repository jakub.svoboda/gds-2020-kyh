﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CountdownManager : MonoBehaviour
{
    private TMP_Text countdownText;

    [SerializeField] private float period = 1f;

    [SerializeField] private string[] texts;
    private int index;

    private float currentTime;

    // Start is called before the first frame update
    void OnEnable()
    {
        countdownText = GetComponent<TMP_Text>();
        currentTime = 0f;

        index = 0;
        countdownText.enabled = true;
        countdownText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > period)
        {
            currentTime -= period;

            if (index >= texts.Length)
            {
                // We are done
                enabled = false;
                countdownText.enabled = false;
                GameManager.Instance.OnGameStart();
                return;
            }

            countdownText.text = texts[index];
            index++;
        }
    }
}
