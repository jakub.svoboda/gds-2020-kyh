﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    public Vector2 resolution;
    public int pixelsToUnit;

    // Start is called before the first frame update
    void Start()
    {
        UpdateSize();
    }

    private void OnEnable()
    {
        UpdateSize();
    }

    void UpdateSize()
    {
        Camera camera = GetComponent<Camera>();

        float scale = Screen.height / resolution.y;
        float scaledPixelsToUnit = pixelsToUnit * scale;
        camera.orthographicSize = (Screen.height / 2.0f) / scaledPixelsToUnit;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
