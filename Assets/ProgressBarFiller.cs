﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ProgressBarFiller : MonoBehaviour
{

    [Range(-1, 1)]
    public float progress;

    public Image image;

    public Sprite positive;
    public Sprite negative;

    public TMP_Text captionText;

    public int index;

    private float lastValue;

    private void OnEnable()
    {
        ThemeSettings settings = GameManager.Instance.LevelManager.currentLevel.themeSettings[index];
        ThemeProgressHolder progressHolder = GameManager.Instance.LevelManager.progressHolders[index];
        captionText.text = settings.name;
    }

    private void Update()
    {
        ThemeSettings settings = GameManager.Instance.LevelManager.currentLevel.themeSettings[index];
        ThemeProgressHolder progressHolder = GameManager.Instance.LevelManager.progressHolders[index];

        if (progressHolder.progressValue != lastValue)
        {
            UpdateProgress(progressHolder.progressValue);
            lastValue = progressHolder.progressValue;
        }
    }

    private void OnValidate()
    {
        UpdateProgress(progress);
    }

    public void UpdateProgress(float progress)
    {
        if (progress < 0)
        {
            image.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 180));
            image.sprite = negative;
        }
        else
        {
            image.gameObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            image.sprite = positive;
        }

        image.fillAmount = Mathf.Abs(progress);
    }
}
