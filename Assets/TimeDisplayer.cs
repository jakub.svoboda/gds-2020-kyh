﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeDisplayer : MonoBehaviour
{
    public TMP_Text timeText;

    void Update()
    {
        float time = Mathf.Max(0f, GameManager.Instance.LevelManager.TotalLevelDuration -
            GameManager.Instance.LevelManager.TotalSecondsEllapsed);
            
        int minutes = (int)time / 60;
        int seconds = (int)(time - 60 * minutes);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

}
