using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScaler : MonoBehaviour
{
    public float duration;

    private float passed;

    private RectTransform rect;

    public bool easingIn = true;

    public bool isEasing;

    private Image bar;

    private void Start()
    {
        bar = GetComponent<Image>();
    }

    public void EaseIn()
    {
        isEasing = true;
        rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.zero;

        passed = 0;
        easingIn = true;
        enabled = true;
    }

    public void EaseOut()
    {
        isEasing = true;
        rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.one;

        passed = 0;
        easingIn = false;
        enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        passed = Mathf.Min(passed + Time.deltaTime, duration);

        if (easingIn)
        {
            rect.localScale = Vector3.one * EasingFunctions.easeInExpo(passed / duration);
        }
        else
        {
            rect.localScale = Vector3.one * (1 - EasingFunctions.easeOutExpo(passed / duration));
        }

        if (passed >= duration)
        {
            if (!easingIn)
            {
                bar.enabled = false;
            }
            enabled = false;
            isEasing = false;
        }
    }
}
