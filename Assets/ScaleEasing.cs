﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleEasing : MonoBehaviour
{
    public float duration;

    [SerializeField]
    private float passed;

    private RectTransform rect;

    bool isEasing = false;

    private void OnEnable()
    {
        if (!isEasing)
            EaseIn();
    }

    public void EaseIn()
    {
        isEasing = true;
        Debug.Log("Easing in!");
        rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.zero;

        passed = 0;
        enabled = true;
    }


    // Update is called once per frame
    void Update()
    {
        rect = gameObject.GetComponent<RectTransform>();
        passed = Mathf.Min(passed + 0.01666666666f, duration);

        rect.localScale = Vector3.one * EasingFunctions.easeInExpo(passed / duration);

        if (passed >= duration)
        {
            enabled = false;
            isEasing = false;
        }
    }
}
