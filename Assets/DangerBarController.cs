﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DangerBarController : MonoBehaviour
{
    public Image[] bars;

    public Color disabledColor;

    public Color normalColor;

    public Color dangerColor;

    public Color warningColor;

    public Image dangerImage;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        int danger = GameManager.Instance.LevelManager.danger;

        Color color = normalColor;

        if (danger < 2)
        {
            color = normalColor;
        }
        else if (danger <= 5)
        {
            color = warningColor;
        }
        else
        {
            color = dangerColor;
        }

        int barIndex = danger / 3 - 1;
        for (int i = 0; i < 3; i++)
        {
            if (i <= barIndex)
            {
                bars[i].color = color;

                // Enable it using easing if necessary
                //if (!bars[i].enabled)
                //{
                //    BarScaler scaler = bars[i].GetComponent<BarScaler>();
                //    scaler.EaseIn();
                //    bars[i].enabled = true;
                //}
            }
            else
            {
                bars[i].color = disabledColor;

                //BarScaler scaler = bars[i].GetComponent<BarScaler>();
                //
                //if (bars[i].enabled && !scaler.isEasing)
                //{
                //    scaler.EaseOut();
                //}
            }
        }

        if (danger > 5)
        {
            dangerImage.enabled = true;
        }
        else
        {
            dangerImage.enabled = false;
        }
    }
}
