﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ClickMusicPlayer : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private string soundName;

    private bool playing = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (soundName != null)
        {
            Debug.Log("Trying to play music!");
            playing = !playing;
            AudioManager.Instance.PlayMusic(soundName);

            if (!playing)
            {
                AudioManager.Instance.StopMusic(soundName);
            }
        }
    }
}
