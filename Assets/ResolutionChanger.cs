﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResolutionChanger : MonoBehaviour
{

    int currentResolution = 0;
    bool fullscreen = false;

    // Start is called before the first frame update
    void Start()
    {
        Screen.SetResolution(1280, 720, fullscreen);
    }

    public void ChangeResolution(int choice)
    {
        switch (choice)
        {
            case 0:
                Screen.SetResolution(1920, 1080, true);
                break;
            case 1:
                Screen.SetResolution(1280, 720, fullscreen);
                break;
            case 3:
                Screen.SetResolution(640, 360, false);
                break;
            default:
                Screen.SetResolution(1920, 1080, true);
                break;
        }
    }

    public void ChangeFullscreen(bool fullscreen)
    {
        this.fullscreen = fullscreen;
        ChangeResolution(currentResolution);
    }
}
