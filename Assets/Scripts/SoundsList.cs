using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Audio/SoundsList")]
public class SoundsList : ScriptableObject
{

    [SerializeField]
    public Sound[] Sounds;

    public Sound GetSound(string name)
    {
        return Array.Find(Sounds, sound => sound.Name == name);
    }

}