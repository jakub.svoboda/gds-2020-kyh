﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestionableTopicResult
{
    public QuestionableTopic Topic;
    public bool statementATrue;
    public bool statementBTrue;
}
