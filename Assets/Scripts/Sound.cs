using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound
{
    public string Name;

    public AudioClip audioClip;

    public bool Loop;

    [Range(0, 1f)]
    public float Pitch = 1;

    [Range(.1f, 3f)]
    public float Volume = 1;

    [HideInInspector]
    public AudioSource Source;


}