﻿using System;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager instance;

    public static AudioManager Instance => instance;

    [SerializeField]
    private SoundsList sfx;


    [SerializeField]
    private SoundsList music;

    [SerializeField]
    private AudioMixerGroup sfxAudioMixer;

    [SerializeField]
    private AudioMixerGroup musicAudioMixer;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        // TODO: Audio source could be created once it is first played 

        foreach (Sound s in sfx.Sounds)
        {
            s.Source = gameObject.AddComponent<AudioSource>();
            s.Source.clip = s.audioClip;
            s.Source.volume = s.Volume;
            s.Source.pitch = s.Pitch;
            s.Source.loop = s.Loop;
            s.Source.outputAudioMixerGroup = sfxAudioMixer;
        }

        foreach (Sound m in music.Sounds)
        {
            m.Source = gameObject.AddComponent<AudioSource>();
            m.Source.clip = m.audioClip;
            m.Source.volume = m.Volume;
            m.Source.pitch = m.Pitch;
            m.Source.loop = m.Loop;
            m.Source.outputAudioMixerGroup = musicAudioMixer;
        }
    }

    public Sound GetSound(string name)
    {
        return sfx.GetSound(name);
    }

    public Sound GetMusic(string name)
    {
        return music.GetSound(name);
    }

    public void Play(string soundName)
    {
        Sound foundSound = sfx.GetSound(soundName);

        if (foundSound == null)
        {
            Debug.LogError("Sound not found: " + soundName);
            return;
        }

        foundSound.Source.loop = foundSound.Loop;
        foundSound.Source.Play();
    }

    public void Stop(string name)
    {
        Sound foundSound = sfx.GetSound(name);

        if (foundSound == null)
        {
            Debug.LogError("Sound not found: " + name);
            return;
        }

        Stop(foundSound);
    }

    public void StopMusic(string name)
    {
        Sound foundSound = music.GetSound(name);

        if (foundSound == null)
        {
            Debug.LogError("Sound not found: " + name);
            return;
        }

        Stop(foundSound);
    }

    public void Stop(Sound sound)
    {
        sound.Source.Play();

        // If looping than stop and let it finish
        if (sound.Loop)
        {
            sound.Source.loop = false;
        }
        // Else stop immediately
        else
        {
            sound.Source.Stop();
        }
    }

    public void PlayMusic(string name)
    {
        Sound foundSound = music.GetSound(name);

        if (foundSound == null)
        {
            Debug.LogError("Sound not found: " + name);
            return;
        }

        foundSound.Source.Play();
    }
}
