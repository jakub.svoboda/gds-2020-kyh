﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ClickSoundPlayer : MonoBehaviour, IPointerClickHandler
{

    [SerializeField]
    private string soundName;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (soundName != null)
        {
            Debug.Log("Click!");
            AudioManager.Instance.Play(soundName);
        }
    }

}
