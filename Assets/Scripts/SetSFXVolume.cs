﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SetSFXVolume : MonoBehaviour
{
    [SerializeField]
    private AudioMixer mixer;

    public void SetVolume(float sliderValue)
    {
        mixer.SetFloat("SfxVolume", Mathf.Log10(sliderValue) * 20);
    }
}
