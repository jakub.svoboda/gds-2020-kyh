using UnityEngine; 

[CreateAssetMenu(menuName = "GameSettings")]
public class GameSettings : ScriptableObject {
    public float SomeSpeed;
    public float WhateverContstant;
    public LevelSpan[] LevelSpans;
    public int normalStatementsCountAtStart = 6;
    public int StatementsToUse = 25;
    public int TopicsToUse = 4;
    public QuestionableTopic[] Topics;
}