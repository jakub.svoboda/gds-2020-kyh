using UnityEngine;

using System;
using System.Linq;
using System.Collections.Generic;

public class SentenceSelector
{
    private ThemeSettings themeSettings;

    private List<int> positiveRunningIndex = new List<int>();
    private List<int> negativeRunningIndex = new List<int>();

    public SentenceSelector(ThemeSettings themeSettings)
    {
        this.themeSettings = themeSettings;
    }

    public string Select(bool isTrue)
    {
        string[] sentences;
        int index;

        if (isTrue)
        {
            sentences = themeSettings.TrueSentences;

            if (positiveRunningIndex.Count == 0)
            {
                int length = themeSettings.TrueSentences.Length;

                // Generate new positive running index
                positiveRunningIndex = GenerateNewIndex(length);
            }

            index = positiveRunningIndex[0];
            positiveRunningIndex.RemoveAt(0);
        }
        else
        {
            sentences = themeSettings.FalseSentences;

            if (negativeRunningIndex.Count == 0)
            {
                int length = themeSettings.FalseSentences.Length;

                // Generate new positive running index
                negativeRunningIndex = GenerateNewIndex(length);
            }

            index = negativeRunningIndex[0];
            negativeRunningIndex.RemoveAt(0);
        }

        // Regenerate the running index
        return sentences[index];
    }

    public List<int> GenerateNewIndex(int length)
    {
        List<int> newList = new List<int>();
        for (int i = 0; i < length; i++)
        {
            newList.Add(i);
        }

        return newList.OrderBy(x => Guid.NewGuid()).ToList();
    }
}