using UnityEngine;

[CreateAssetMenu(fileName = "ThemeSettings", menuName = "ThemeSettings", order = 0)]
public class ThemeSettings : ScriptableObject
{
    public string Name;

    public string[] TrueSentences;

    public string[] FalseSentences;

    public string[] NeutralSentences;
}