
[System.Serializable]
public class ThemeProgressHolder
{
    public ThemeSettings theme;
    public int balance;
    public int trueBalance;
    public int falseBalance;

    public float progressValue;
}