﻿using UnityEngine;

[CreateAssetMenu(menuName = "GameStatements")]
public class GameStatements: ScriptableObject
{
    public string[] True;
    public string[] False;
}
