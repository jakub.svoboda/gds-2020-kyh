using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverManager : MonoBehaviour
{
    List<QuestionableTopicResult> results;
    int index = 0;

    public TMP_Text titleText;
    public TMP_Text messageText;

    public TMP_Text statusText;

    public TMP_Text sentence1;
    public TMP_Text sentence2;

    public TMP_Text gameOverText;

    public GameObject exitButton;
    public GameObject restartButton;
    public GameObject continueButton;

    public Color trueColor;
    public Color falseColor;

    private void Start()
    {
    }

    public void HandeResults(List<QuestionableTopicResult> topicResults)
    {
        statusText.text = "Game finished!";
        gameOverText.enabled = false;
        index = 0;
        results = topicResults;
        NextTopic();
    }

    public void GameOver()
    {
        gameObject.SetActive(true);
        continueButton.SetActive(false);
        restartButton.SetActive(true);
        exitButton.SetActive(true);

        statusText.text = "Game over!";
        gameOverText.enabled = true;
        messageText.text = "";
        titleText.text = "";
        sentence1.text = "";
        sentence2.text = "";
    }

    public void NextTopic()
    {
        // If I am at the end
        if (index == results.Count - 1)
        {
            continueButton.SetActive(false);
            restartButton.SetActive(true);
            exitButton.SetActive(true);
        }
        else
        {
            continueButton.SetActive(true);
            restartButton.SetActive(false);
            exitButton.SetActive(false);
        }

        QuestionableTopicResult current = results[index];
        index++;

        titleText.text = current.Topic.Heading;

        string message = "";
        if (current.statementATrue && current.statementBTrue)
        {
            message = current.Topic.TrueTrue;
        }
        else if (!current.statementATrue && current.statementBTrue)
        {
            message = current.Topic.FalseTrue;
        }
        else if (current.statementATrue && !current.statementBTrue)
        {
            message = current.Topic.TrueFalse;
        }
        else
        {
            message = current.Topic.FalseFalse;
        }

        sentence1.text = current.Topic.statementA;
        if (current.statementATrue)
        {
            sentence1.color = trueColor;
        }
        else
        {
            sentence1.color = falseColor;
        }

        sentence2.text = current.Topic.statementB;
        if (current.statementBTrue)
        {
            sentence2.color = trueColor;
        }
        else
        {
            sentence2.color = falseColor;
        }

        messageText.text = message;
    }
}