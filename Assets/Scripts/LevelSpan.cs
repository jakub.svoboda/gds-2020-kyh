﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelSpan
{
    public int Duration;
    public int BirdsToSpawn;
    public float lifespanMin;
    public float lifespanMax;
}
