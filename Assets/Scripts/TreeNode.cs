using UnityEngine;

[CreateAssetMenu(fileName = "TreeNode", menuName = "TreeNode", order = 0)]
public class TreeNode : ScriptableObject
{
    public TreeNode nextTrue;

    public TreeNode nextFalse;

    [TextArea]
    public string sentence;

    public bool isEnding()
    {
        return nextTrue == null && nextFalse == null;
    }

    public TreeNode NextNode(bool isTrue)
    {
        TreeNode newNode = null;
        if (isTrue)
        {
            newNode = nextTrue;
        }

        newNode = nextFalse;

        if (newNode == null)
        {
            Debug.LogError("Null tree node on " + name);
        }

        return newNode;
    }
}