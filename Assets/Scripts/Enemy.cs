using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enemy : MonoBehaviour
{
    public bool isEmerged = false;

    public ThemeSettings theme;

    public StatementType statementType;

    public string sentence;

    private LevelManager levelManager;

    public TMP_Text text;

    public List<GameObject> toDestroy = new List<GameObject>();

    public float lifespan;

    public Animator animator;
    public bool wasStriked = false;

    public void Initialize(LevelManager levelManager, ThemeSettings theme, string sentence, StatementType statementType, float lifespan)
    {
        this.levelManager = levelManager;
        this.theme = theme;
        this.sentence = sentence;
        this.statementType = statementType;
        this.lifespan = lifespan;
        this.animator = GetComponent<Animator>();
        //text.text = sentence;
    }

    public void OnStrike()
    {
        levelManager.Striked(this);
        wasStriked = true;
        foreach (GameObject obj in toDestroy)
        {
            Destroy(obj);
        }
        animator.SetTrigger("strike");

        string clip = string.Empty;
        if(statementType == StatementType.True)
        {
            clip = "TrueHit";
        }
        else
        {
            string[] whacks = { "Whack1", "Whack4", "Whack6" };
            int range = Random.Range(0, whacks.Length);
            clip = whacks[range];
        }
        AudioManager.Instance.Play(clip);
    }

    private void OnDestroy()
    {
        foreach (GameObject obj in toDestroy)
        {
            if (obj != null)
            {
                Destroy(obj);
            }
        }
    }

    public void OnPass()
    {
        foreach (GameObject obj in toDestroy)
        {
            Destroy(obj);
        }
        animator.SetTrigger("disappear");
    }

    private void Start()
    {

    }

    private void Update()
    {
        if (!isEmerged)
        {
            return;
        }

        lifespan -= Time.deltaTime;
        if (lifespan <= 0f)
        {
            OnPass();
        }
    }

    public void onAnimationEvent(string eventName)
    {
        if (eventName == "emerged")
        {
            if (wasStriked)
            {
                return;
            }

            levelManager.OnEnemyEmerged(this);
            isEmerged = true;

            if (name == "TopicEnemy(Clone)")
            {
                AudioManager.Instance.Play("NewTweet2");
            }
            else
            {
                AudioManager.Instance.Play("NewTweet");
            }
        }
        else if (eventName == "whacked")
        {
            CleanDestroy();
        }
        else if (eventName == "disappeared")
        {
            levelManager.Passed(this);
            CleanDestroy();
        }
        else
        {
            Debug.Assert(false, $"Unkwnown eventName: {eventName}");
        }
    }

    private void CleanDestroy()
    {
        foreach (GameObject obj in toDestroy)
        {
            Destroy(obj);
        }

        Destroy(gameObject);
    }
}