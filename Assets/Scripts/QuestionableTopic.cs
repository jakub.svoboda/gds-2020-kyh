﻿using UnityEngine;

[CreateAssetMenu(menuName = "QuestionableTopic")]
public class QuestionableTopic : ScriptableObject
{
    public string Heading = string.Empty;

    [Header("Statements")]
    public string statementA = string.Empty;
    public string statementB = string.Empty;

    [Header("Result combinations")]
    [TextArea]
    public string TrueTrue = string.Empty;
    [TextArea]
    public string TrueFalse = string.Empty;
    [TextArea]
    public string FalseTrue = string.Empty;
    [TextArea]
    public string FalseFalse = string.Empty;
}
