﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverSoundPlayer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool mouseOver;

    [SerializeField]
    private string enterSound;

    [SerializeField]
    private string exitSound;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        mouseOver = true;

        AudioManager audioManager = AudioManager.Instance;
        Sound sound = audioManager.GetSound(enterSound);

        Debug.Log("On POINTER ENTER.");

        if (enterSound != null)
        {
            audioManager.Play(enterSound);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        mouseOver = false;

        Debug.Log("On POINTER EXIT.");

        AudioManager audioManager = AudioManager.Instance;
        Sound startSound = audioManager.GetSound(enterSound);

        // If the starting sound is looping, then stop it 
        if (startSound != null && startSound.Loop)
        {
            audioManager.Stop(startSound);
        }
        // Else play exit sfx if we have some
        else if (exitSound != null)
        {
            audioManager.Play(exitSound);
        }
    }
}
