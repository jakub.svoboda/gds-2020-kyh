using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum StatementType
{
    True,
    False,
    Questionable
}

public class LevelManager : MonoBehaviour
{
    private struct Statement
    {
        public string Text;
        public StatementType Type;

        public Statement(string text, StatementType type)
        {
            Text = text;
            Type = type;
        }
    }
    public Level[] levels;

    public Level currentLevel;

    public GameStatements statements;

    public List<SentenceSelector> selectors = new List<SentenceSelector>();

    public ThemeProgressHolder[] progressHolders;

    public GameObject enemyPrefab;
    public GameObject topicEnemyPrefab;
    public GameObject enemyUIPrefab;
    public RectTransform enemiesUIHolder;
    public RectTransform canvasRect;

    public Transform enemiesHolder;

    public TilemapInteractor tilemapInteractor;
    private Enemy[,] tiles;

    public float TotalSecondsEllapsed = 0;
    public float TotalLevelDuration;
    public int TotalLevelBirdsCount;
    public int AlreadySpawnedBirdsCount;

    private LevelSpan[] spans;
    private LevelSpan span;
    private int spanIndex = 0;
    private float spawnRate = 0f;
    private float spawnTimer = 0f;
    private int spanEnd;

    private bool waitingForNoBirdsLeft = false;
    private int birdsLeft = 0;

    private (int X, int Y)[] randomTraversalIndices;
    private int traversalIndex = 0;
    private Statement[] statementSequence;
    private int statementIndex = 0;
    private Dictionary<string, (QuestionableTopicResult Result, bool isA)> statementToTopicResult;

    private GameSettings settings => GameManager.Instance.gameSettings;



    void Awake()
    {
        tilemapInteractor.Interacted += OnTilemapInteracted;
        int dim = tilemapInteractor.Dimension;
        tiles = new Enemy[dim, dim];
        randomTraversalIndices = randomTraversalIndices = new (int X, int Y)[dim * dim];
    }

    private void OnTilemapInteracted(int x, int y)
    {
        if (tiles[x, y] is Enemy enemy)
        {
            enemy.OnStrike();
            tiles[x, y] = null;
        }
    }

    public void OnEnemyEmerged(Enemy enemy)
    {
        // Spawn the UI element
        GameObject enemyUIGo = Instantiate(enemyUIPrefab);
        RectTransform enemyRectTransform = enemyUIGo.GetComponent<RectTransform>();

        // Show the text
        TMP_Text enemySentenceText = enemyUIGo.GetComponent<TMP_Text>();
        enemySentenceText.text = enemy.sentence;

        Vector2 ViewportPosition = mainCamera.WorldToViewportPoint(enemy.gameObject.transform.position);
        Vector2 WorldObject_ScreenPosition = new Vector2(
            ((ViewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((ViewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));

        Debug.Log("Viewport " + ViewportPosition.x + " " + ViewportPosition.y);
        Debug.Log("Canvas rect " + canvasRect.sizeDelta.x + " " + canvasRect.sizeDelta.y);

        Vector2 proportionalPosition = new Vector2(ViewportPosition.x * canvasRect.sizeDelta.x, ViewportPosition.y * canvasRect.sizeDelta.y);
        Vector2 uiOffset = new Vector2((float)canvasRect.sizeDelta.x / 2f, (float)canvasRect.sizeDelta.y / 2f);

        // Set the position and remove the screen offset
        //enemyRectTransform.localPosition = proportionalPosition - uiOffset;

        enemyRectTransform.SetParent(enemiesUIHolder);
        //enemyRectTransform.localPosition = WorldObject_ScreenPosition;
        enemyRectTransform.anchoredPosition = WorldObject_ScreenPosition;
        enemyRectTransform.localScale = Vector3.one;

        // Add it to be destroyed when enemy is destroyed
        enemy.toDestroy.Add(enemyUIGo);
    }

    public Camera mainCamera;

    public void StartLevel()
    {
        AlreadySpawnedBirdsCount = 0;
        birdsLeft = 0;
        waitingForNoBirdsLeft = false;

        // Setup shuffled topics
        statementToTopicResult = new Dictionary<string, (QuestionableTopicResult Result, bool isA)>();
        var shuffledTopics = new QuestionableTopic[settings.Topics.Length];
        System.Array.Copy(settings.Topics, shuffledTopics, shuffledTopics.Length);
        Shuffle(shuffledTopics);

        // Setup random statement traversal
        int topicStatementsCount = settings.TopicsToUse * 2;
        Debug.Assert(settings.StatementsToUse - topicStatementsCount >= settings.normalStatementsCountAtStart, "There is too many normal statements at the start!");

        // shuffle the topic statements
        Statement[] topicStatements = new Statement[topicStatementsCount];
        for (int i = 0; i < settings.TopicsToUse; i++)
        {
            var topic = shuffledTopics[i];
            var topicResult = new QuestionableTopicResult() { Topic = topic, statementATrue = false, statementBTrue = false };
            topicStatements[2*i] = new Statement(topic.statementA, StatementType.Questionable);
            statementToTopicResult.Add(topic.statementA, (topicResult, true));
            topicStatements[2*i + 1] = new Statement(topic.statementB, StatementType.Questionable);
            statementToTopicResult.Add(topic.statementB, (topicResult, false));
        }
        Shuffle(topicStatements);

        // shuffle the normal statements
        Statement[] normalStatements = new Statement[statements.True.Length + statements.False.Length];
        for (int i = 0; i < statements.True.Length; i++)
        {
            var statement = statements.True[i];
            normalStatements[i] = new Statement(statement, StatementType.True);
        }
        for (int i = 0; i < statements.False.Length; i++)
        {
            var statement = statements.False[i];
            normalStatements[i + statements.True.Length] = new Statement(statement, StatementType.False);
        }
        Shuffle(normalStatements);

        // pick start statements
        Statement[] startStatements = new Statement[settings.normalStatementsCountAtStart];
        for (int i = 0; i < settings.normalStatementsCountAtStart; i++)
        {
            startStatements[i] = normalStatements[i];
        }

        // shufle non start statements
        Statement[] nonStartStatements = new Statement[settings.StatementsToUse - settings.normalStatementsCountAtStart];
        for (int i = 0; i < topicStatementsCount; i++)
        {
            nonStartStatements[i] = topicStatements[i];
        }
        for (int i = 0; i < settings.StatementsToUse - settings.normalStatementsCountAtStart - topicStatementsCount; i++)
        {
            nonStartStatements[i + topicStatementsCount] = normalStatements[i + settings.normalStatementsCountAtStart];
        }
        Shuffle(nonStartStatements);

        // bring all the statements together
        statementSequence = new Statement[settings.StatementsToUse];
        statementIndex = 0;
        for (int i = 0; i < startStatements.Length; i++)
        {
            statementSequence[i] = startStatements[i];
        }
        for (int i = 0; i < nonStartStatements.Length; i++)
        {
            statementSequence[i + startStatements.Length] = nonStartStatements[i];
        }

        // Setup random spawn traversal (generate all indices and then shuffle)
        traversalIndex = 0;
        int index = 0;
        for (int x = 0; x < tilemapInteractor.Dimension; ++x)
        {
            for (int y = 0; y < tilemapInteractor.Dimension; ++y)
            {
                randomTraversalIndices[index] = (x, y);
                index++;
            }
        }
        Shuffle(randomTraversalIndices);

        // Setting up level span related stuff
        TotalSecondsEllapsed = 0;
        spans = settings.LevelSpans;
        if (spans.Length == 0)
        {
            spanIndex = -1;
        }
        else
        {
            spanIndex = 0;
            span = spans[spanIndex];
            spawnRate = (float)span.Duration / span.BirdsToSpawn;
            spanEnd = span.Duration;
        }
        TotalLevelDuration = 0;
        TotalLevelBirdsCount = 0;
        for (int i = 0; i < spans.Length; i++)
        {
            TotalLevelBirdsCount += spans[i].BirdsToSpawn;
            TotalLevelDuration += spans[i].Duration;
        }

        // Select a new level
        int choice = Random.Range(0, levels.Length);
        currentLevel = levels[choice];

        foreach (var setting in currentLevel.themeSettings)
        {
            selectors.Add(new SentenceSelector(setting));
        }

        progressHolders = new ThemeProgressHolder[currentLevel.themeSettings.Length];
        for (int i = 0; i < currentLevel.themeSettings.Length; i++)
        {
            ThemeProgressHolder progressHolder = new ThemeProgressHolder();
            progressHolder.theme = currentLevel.themeSettings[i];
            progressHolders[i] = progressHolder;
        }
    }

    private static void Shuffle<T>(T[] array)
    {
        int n = array.Length;
        while (n > 1)
        {
            int k = Random.Range(0, n);
            n--;
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }

    // Called when the game should start
    public void StartGame()
    {
        enabled = true;
        tilemapInteractor.ShouldInteract = true;
    }

    public void EndGame()
    {
        enabled = false;
        tilemapInteractor.ShouldInteract = false;
    }

    private void Update()
    {
        Debug.Assert(spanIndex != -1, "No level spans are set up in the game settings!");

        if (danger >= maxDanger)
        {
            GameManager.Instance.gameOverManager.GameOver();
            return;
        }

        if (waitingForNoBirdsLeft)
        {
            if (birdsLeft == 0)
            {
                EndLevel();
            }
            return;
        }

        // increment timers
        TotalSecondsEllapsed += Time.deltaTime;
        spawnTimer += Time.deltaTime;

        if (statementIndex >= statementSequence.Length)
        {
            waitingForNoBirdsLeft = true;
            return;
        }

        // check for bird spawn
        if (spawnTimer >= spawnRate)
        {
            spawnTimer -= spawnRate;
            SpawnEnemy();
        }

        // check if we entered a new span
        if (TotalSecondsEllapsed > spanEnd && spanIndex < spans.Length - 1)
        {
            spanIndex++;
            Debug.Log("Switched to span: " + spanIndex);
            if (spanIndex >= spans.Length)
            {
                waitingForNoBirdsLeft = true;
            }
            else
            {
                span = spans[spanIndex];
                spawnRate = (float)span.Duration / span.BirdsToSpawn;
                spanEnd = spanEnd + span.Duration;
            }
        }
    }

    public void EndLevel()
    {
        enabled = false;
        tilemapInteractor.ShouldInteract = false;
        HashSet<QuestionableTopicResult> uniqueResults = new HashSet<QuestionableTopicResult>();
        foreach (var entry in statementToTopicResult)
        {
            uniqueResults.Add(entry.Value.Result);
        }
        GameManager.Instance.gameOverManager.HandeResults(new List<QuestionableTopicResult>(uniqueResults));
        GameManager.Instance.EndLevel();
    }

    public void SpawnEnemy()
    {
        var statement = statementSequence[statementIndex];
        statementIndex++;

        StatementType statementType = statement.Type;
        string sentence = statement.Text;
        float lifespan;
        if (statement.Type == StatementType.Questionable)
        {
            lifespan = Random.Range(span.lifespanMin, span.lifespanMax) + 3; // Always 3 more seconds for questionable
        }
        else
        {
            lifespan = Random.Range(span.lifespanMin, span.lifespanMax);
        }

        // Spawn the enemy 
        GameObject enemyGO = null;
        if (statementType == StatementType.Questionable)
        {
            enemyGO = Instantiate(topicEnemyPrefab);
        }
        else
        {
            enemyGO = Instantiate(enemyPrefab);
        }
        enemyGO.transform.SetParent(enemiesHolder);
        enemyGO.transform.localPosition = Vector3.zero;
        Enemy enemy = enemyGO.GetComponent<Enemy>();
        enemy.transform.SetParent(transform);

        int choice = Random.Range(0, currentLevel.themeSettings.Length);
        ThemeSettings themeSettings = currentLevel.themeSettings[choice];

        int current = traversalIndex;
        do
        {
            current++;
            if (current >= randomTraversalIndices.Length)
            {
                current = 0;
            }
            if (tiles[randomTraversalIndices[current].X, randomTraversalIndices[current].Y] == null)
            {
                break;
            }
            if (current == traversalIndex)
            {
                //TODO: what to do when everything is full, will this ever happen?
                current = traversalIndex - 1;
                if (current < 0)
                {
                    current = randomTraversalIndices.Length - 1;
                }
                break;
            }
        } while (true);
        traversalIndex = current;
        (int X, int Y) coord = randomTraversalIndices[traversalIndex];
        Vector3 gridPosition = tilemapInteractor.GetTileCenterPosition(coord.X, coord.Y);
        enemyGO.transform.position = gridPosition;
        tiles[coord.X, coord.Y] = enemy;
        
        enemy.Initialize(this, themeSettings, sentence, statementType, lifespan);

        birdsLeft++;
        AlreadySpawnedBirdsCount++;
    }

    public void Striked(Enemy enemy)
    {
        ThemeProgressHolder progress = null;
        int index = 0;
        for (; index < progressHolders.Length; index++)
        {
            if (progressHolders[index].theme == enemy.theme)
            {
                progress = progressHolders[index];
                break;
            }
        }

        if (progress == null)
        {
            Debug.LogError("Didn't find the progress " + enemy.theme.name);
            return;
        }

        switch (enemy.statementType)
        {
            case StatementType.True:
                progress.balance--;
                progress.trueBalance--;
                OnHit(true);
                break;
            case StatementType.False:
                progress.balance++;
                progress.falseBalance--;
                OnHit(false);
                break;
            case StatementType.Questionable:
                var entry = statementToTopicResult[enemy.sentence];
                if (entry.isA)
                {
                    entry.Result.statementATrue = false;
                }
                else
                {
                    entry.Result.statementBTrue = false;
                }
                break;
        }

        progressHolders[index].progressValue = (float)progress.balance / (TotalLevelBirdsCount / 2.0f);
        birdsLeft--;
    }

    public void Passed(Enemy enemy)
    {
        ThemeProgressHolder progress = null;
        int index = 0;
        for (; index < progressHolders.Length; index++)
        {
            if (progressHolders[index].theme == enemy.theme)
            {
                progress = progressHolders[index];
                break;
            }
        }

        if (progress == null)
        {
            Debug.LogError("Didn't find the progress " + enemy.theme.name);
            return;
        }
        switch (enemy.statementType)
        {
            case StatementType.True:
                progress.balance++;
                progress.trueBalance++;
                OnPass(true);
                break;
            case StatementType.False:
                progress.balance--;
                progress.falseBalance++;
                OnPass(false);
                break;
            case StatementType.Questionable:
                var entry = statementToTopicResult[enemy.sentence];
                if (entry.isA)
                {
                    entry.Result.statementATrue = true;
                }
                else
                {
                    entry.Result.statementBTrue = true;
                }
                break;
        }

        // Update the progress barr
        progressHolders[index].progressValue = (float)progress.balance / (TotalLevelBirdsCount / 2.0f);
        birdsLeft--;
    }

    public void OnLevelTransitioned()
    {
        // TODO:
    }

    #region danger

    [Header("Danger:")]

    public int danger;

    public int dangerGain = 3;

    public int maxDanger = 9;

    public void OnHit(bool isTrue)
    {
        if (isTrue)
        {
            danger = Mathf.Min(danger + dangerGain, maxDanger);
        }
        else
        {
            danger = Mathf.Max(danger - 1, 0);
        }
    }

    public void OnPass(bool isTrue)
    {
        if (isTrue)
        {
            danger = Mathf.Max(danger - 1, 0);
        }
        else
        {
            danger = Mathf.Min(danger + dangerGain, maxDanger);
        }
    }
    #endregion
}
