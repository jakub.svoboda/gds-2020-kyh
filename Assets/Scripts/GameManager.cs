using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    public static GameManager Instance => instance;

    public Camera mainCamera;

    public GameSettings gameSettings;

    public GameObject tutorialHolder;

    public GameObject UICanvas;
    public GameObject GameCanvas;

    [HideInInspector]
    public LevelManager LevelManager;

    public GameObject levelPrefab;

    public GameObject gameOver;

    public UIManager uIManager;

    public RectTransform canvasRect;

    public RectTransform enemiesUIHolder;
    public GameOverManager gameOverManager;

    private string transitioning;

    private void Start()
    {
        instance = this;
    }

    public void OnFadeDone()
    {
        if (transitioning == "ToLevel")
        {
            UICanvas.SetActive(false);
            GameCanvas.SetActive(true);

            tutorialHolder.SetActive(true);
        }
        else if (transitioning == "Restart")
        {
            Destroy(LevelManager.gameObject);
            gameOver.SetActive(false);

            InitNewLevel();
            tutorialHolder.SetActive(true);

            UICanvas.SetActive(false);
            GameCanvas.SetActive(true);
        }
        else if (transitioning == "ToMenu")
        {
            Destroy(LevelManager.gameObject);
            gameOver.SetActive(false);

            UICanvas.SetActive(true);
            GameCanvas.SetActive(false);
        }

        transitioning = null;
    }

    public void StartGame()
    {
        InitNewLevel();

        uIManager.Transition();
        transitioning = "ToLevel";
    }

    private void InitNewLevel()
    {
        GameObject level = Instantiate(levelPrefab);
        level.transform.position = Vector3.zero;
        LevelManager = level.GetComponent<LevelManager>();
        LevelManager.mainCamera = mainCamera;
        LevelManager.canvasRect = canvasRect;
        LevelManager.enemiesUIHolder = enemiesUIHolder;
        LevelManager.StartLevel();
    }

    // Called from the countdown
    public void OnGameStart()
    {
        LevelManager.StartGame();
    }

    public void EndLevel()
    {
        // TODO: Calculate the ending using the level manager
        gameOver.SetActive(true);
        Time.timeScale = 0f;
    }

    // Called when user clicks Restart on the game over screen
    public void OnEndLevelRestart()
    {
        Time.timeScale = 1f;

        uIManager.Transition();
        transitioning = "Restart";
    }

    // Called when user clicks Exit on the game over screen
    public void OnEndLevelExit()
    {
        Time.timeScale = 1f;

        uIManager.Transition();
        transitioning = "ToMenu";
    }
}