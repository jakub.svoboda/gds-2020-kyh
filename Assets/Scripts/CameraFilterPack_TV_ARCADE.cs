﻿////////////////////////////////////////////////////////////////////////////////////
//  CameraFilterPack v2.0 - by VETASOFT 2015 //////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;

public class CameraFilterPack_TV_ARCADE : MonoBehaviour
{
    private float TimeX = 1.0f;

    [SerializeField]
    private Material material;

    void Update()
    {
        TimeX += Time.deltaTime;
        if (TimeX > 100)
        {
            TimeX = 0;
        }

        material.SetFloat("_TimeX", TimeX);
        material.SetVector("_ScreenResolution", new Vector4(Screen.currentResolution.width, Screen.currentResolution.height, 0.0f, 0.0f));
    }
}