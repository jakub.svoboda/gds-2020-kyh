﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapInteractor : MonoBehaviour
{
    public delegate void InteractedHandler(int x, int y);

    public event InteractedHandler Interacted;
    public int Dimension { get; private set; } = 4;
    private bool _shouldInteract = false;
    public bool ShouldInteract
    {
        get => _shouldInteract;
        set
        {
            _shouldInteract = value;
            if(value == false)
            {
                if(prevCoordinates != None)
                {
                    gameplayMap.SetTile(prevCoordinates, normal);
                    prevCoordinates = None;
                }
            }
        }
    }

    [SerializeField] private Tile normal;
    [SerializeField] private Tile higlighted;
    public Tilemap gameplayMap;

    private static readonly Vector3Int None = new Vector3Int(int.MaxValue, -int.MaxValue, int.MaxValue);

    private Vector3Int prevCoordinates = None;

    private int minX = int.MaxValue;
    private int minY = int.MaxValue;
    private int maxX = -int.MaxValue;
    private int maxY = -int.MaxValue;

    private void Awake()
    {
        for (int x = gameplayMap.cellBounds.min.x; x < gameplayMap.cellBounds.max.x; x++)
        {
            for (int y = gameplayMap.cellBounds.min.y; y < gameplayMap.cellBounds.max.y; y++)
            {
                for (int z = gameplayMap.cellBounds.z; z < gameplayMap.cellBounds.max.z; z++)
                {
                    var coords = new Vector3Int(x, y, z);
                    if (gameplayMap.HasTile(coords))
                    {
                        minX = x < minX ? x : minX;
                        minY = y < minY ? y : minY;
                        maxX = x > maxX ? x : maxX;
                        maxY = y > maxY ? y : maxY;
                    }
                }
            }
        }
        Dimension = maxX - minX + 1;
        Debug.Assert(maxY - minY + 1 == Dimension, "The grid should be a rectangle!");
    }


    void Update()
    {
        if (!ShouldInteract)
        {
            return;
        }

        Vector3 mouseWorldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int coords = gameplayMap.WorldToCell(mouseWorldPosition);
        coords.z = 0;
        bool hasTile = gameplayMap.HasTile(coords);
        //Debug.Log($"screen: {Input.mousePosition} | world: {mouseWorldPosition} | tilemap: {coords} | center: {gameplayMap.GetCellCenterWorld(coords)} | hasTile: {hasTile}");
        if (coords != prevCoordinates)
        {
            if (prevCoordinates != None)
            {
                gameplayMap.SetTile(prevCoordinates, normal);
                prevCoordinates = None;
            }
            if (hasTile)
            {
                gameplayMap.SetTile(coords, higlighted);
                prevCoordinates = coords;
            }
            return;
        }

        if (hasTile && Input.GetMouseButtonDown(0))
        {
            Interacted?.Invoke(coords.x - minX, coords.y - minY);
        }
    }

    private Vector3Int GetIndex(int x, int y)
    {
        return new Vector3Int(minX + x, minY + y, 0);
    }

    public TileBase GetTile(int x, int y)
    {
        return gameplayMap.GetTile(GetIndex(x, y));
    }

    public Vector3 GetTileCenterPosition(int x, int y)
    {
        Vector3 result = gameplayMap.GetCellCenterWorld(GetIndex(x, y));
        return result;
    }
}
